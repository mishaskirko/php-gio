<?php

declare(strict_types=1);

namespace App;

class Transaction
{
    /**
     * @param float  $amount
     * @param string $description
     */
    public function __construct(
        private float $amount,
        private readonly string $description
    ) {
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param float $rate
     *
     * @return Transaction
     */
    public function addTax(float $rate): Transaction
    {
        $this->amount += $this->amount * $rate / 100;

        return $this;
    }

    /**
     * @param float $rate
     *
     * @return Transaction
     */
    public function applyDiscount(float $rate): Transaction
    {
        $this->amount -= $this->amount * $rate / 100;

        return $this;
    }

}